const urlUsers = 'https://ajax.test-danit.com/api/json/users';
const urlPosts = 'https://ajax.test-danit.com/api/json/posts';
const currentUser = { name: 'Clementina DuBuque', email: 'Rey.Padberg@karina.biz', userId: 10 };
const mainDiv = document.querySelector('.main');
let postIdCont = 0;
const cardsInsArr = [];

class User {
   constructor(name, email, id) {
      this._name = name
      this._email = email
      this._id = id
   }

   get name() {
      return this._name
   }
   get email() {
      return this._email
   }
   get id() {
      return this._id
   }
   set name(newName) {
      this._name = newName;
   }
   set email(newEmail) {
      this._email = newEmail;
   }
}

class Post {
   constructor(title, body, userId, postId) {
      this._title = title
      this._body = body
      this._userId = userId
      this._postId = postId
   }
   get title() {
      return this._title
   }
   get body() {
      return this._body
   }
   get userId() {
      return this._userId
   }
   get postId() {
      return this._postId
   }
   set title(newTitle) {
      this._title = newTitle;
   }
   set body(newBody) {
      this._body = newBody;
   }
}

class Card {
   constructor(user, post) {
      this._post = post
      this._user = user
   }
   get name() {
      return this._user.name
   }
   get email() {
      return this._user.email
   }
   get title() {
      return this._post.title
   }
   get body() {
      return this._post.body
   }
   get userId() {
      return this._post.userId
   }
   get postId() {
      return this._post.postId
   }
   set title(newTitle) {
      this._post.title = newTitle;
   }
   set body(newBody) {
      this._post.body = newBody;
   }

   addPostOnPage(rootElem = mainDiv) {
      let cardString = `<h2 class="main__card-name">${this.name}</h2>
               <div class="main__card-email"><a href="mailto:${this.email}">${this.email}</a></div>
               <h3 class="main__card-title">${this.title}</h3>
               <p class="main__card-body" style="word-wrap: break-word;">${this.body}</p>
               <div class="main__card-btns">
                  <button class="btn-icon" onclick='deletePost(${this.postId})'>
                     <i class="fa-solid fa-circle-xmark fa-xl"></i>
                  </button>
                  <button class="btn-icon" onclick='editPost(${this.postId})'>
                     <i class="fa-solid fa-pen-to-square fa-xl"></i>
                  </button>
               </div>`;
      rootElem.insertAdjacentHTML('afterbegin', `
               <div class="main__card" id='cardId-${this.postId}'>${cardString}</div>`);
   }

   delete() {
      fetch(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
         method: 'DELETE',
         headers: {
            'Content-Type': 'application/json'
         },
      })
         .then(response => {
            if (response.ok || true) {
               const deleteDiv = document.querySelector(`#cardId-${this.postId}`);
               deleteDiv.remove();
            }
         });
   }

   edit(newTitleValue, newBodyValue) {
      fetch(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
         method: 'PUT',
         headers: {
            'Content-Type': 'application/json',
         },
         body: JSON.stringify({ newTitleValue, newBodyValue }),
      })
         .then(response => {
            if (response.ok || true) {
               this.title = newTitleValue;
               this.body = newBodyValue;

               const editDiv = document.querySelector(`#cardId-${this.postId}`);
               editDiv.querySelector('.main__card-title').innerHTML = newTitleValue;
               editDiv.querySelector('.main__card-body').innerHTML = newBodyValue;
            }
         });
   }
}

function fetchURL(url) {
   return fetch(url)
      .then(response => response.json())
      .catch(error => console.error(error));
};

// Створення єкземплярів класу Юзер
function createUsers(userClass, userUrl) {
   return fetchURL(userUrl)
      .then(data => {
         return data.map(obj => {
            return new userClass(obj.name, obj.email, obj.id)
         })
      })
};

// Створення єкземплярів класу Пост
function createPosts(postClass, postUrl) {
   return fetchURL(postUrl)
      .then(data => {
         return data.map(obj => {
            return new postClass(obj.title, obj.body, obj.userId, obj.id)
         })
      })
};

// Створення єкземплярів класу Кард (виклик далі в функції postCart)
function createCarts(userClass, userUrl, postClass, postUrl) {
   return createUsers(userClass, userUrl)
      .then(users => {
         return createPosts(postClass, postUrl)
            .then(posts => {
               return posts.map(postIns => {
                  let userIns = null;
                  users.forEach(user => {
                     if (user.id === postIns.userId) {
                        userIns = user;
                     }
                  });
                  postIdCont++
                  return new Card(userIns, postIns);
               });
            });
      })
      .catch(err => {
         console.error('Error in createCarts', err);
      });
};

// Публікація на сторінці єкземплярів класу Кард
function postCarts() {
   const mosaicLoader = document.querySelector('.main-mosaic-loader');
   try {
      createCarts(User, urlUsers, Post, urlPosts)
         .then((data) => {
            console.log(data);

            data.forEach((card) => {
               cardsInsArr.push(card);
               mosaicLoader.style.display = "none";
               card.addPostOnPage();
            })
         })
   } catch (error) {
      console.log(error);
   }
};
let startId = setTimeout(postCarts, 1500)

// Видалення постів
function deletePost(postId) {
   cardsInsArr.forEach(cardIns => {
      if (cardIns.postId === postId) {
         cardIns.delete();
         const index = cardsInsArr.indexOf(cardIns);
         if (index !== -1) {
            cardsInsArr.splice(index, 1);
         }
      }
   })
   console.log(cardsInsArr);
};

// Додавання слухачів та констант для модального вікна створення постів
const modalAddPost = document.querySelector('.modal');
const addPostForm = document.querySelector('.add-post-form');
addPostForm.addEventListener('submit', addPost);
const modalBody = document.querySelector('.modal__body');
modalBody.addEventListener('click', (event) => {
   if (!event.target.closest('.modal__content')) {
      modalAddPost.classList.remove('open')
   }
});

function toggleModalAddPost() {
   modalAddPost.classList.toggle('open')
};

// Створення нового посту
function addPost(event) {
   event.preventDefault();
   const title = document.querySelector('#title').value;
   const body = document.querySelector('#body').value;

   fetch('https://ajax.test-danit.com/api/json/posts', {
      method: 'POST',
      headers: {
         'Content-Type': 'application/json'
      },
      body: JSON.stringify({
         title,
         body,
         name: currentUser.name,
         email: currentUser.email,
         userId: currentUser.userId,
      })
   })
      .then(response => {
         if (response.ok) {
            postIdCont++
            const user = new User(currentUser.name, currentUser.email, currentUser.userId);
            const post = new Post(title, body, currentUser.userId, postIdCont);
            const card = new Card(user, post);
            cardsInsArr.push(card);
            card.addPostOnPage();
            console.log(cardsInsArr);

            modalAddPost.classList.remove('open');
            addPostForm.reset();
         }
      })
};

// Додавання слухачів та констант для модального вікна редагування постів
const modalEditPost = document.querySelector('.modal-edit');
const editPostForm = document.querySelector('.edit-post-form');
const modalBodyEdit = document.querySelector('.modal__body-edit');
modalBodyEdit.addEventListener('click', (event) => {
   if (!event.target.closest('.modal__content')) {
      modalEditPost.classList.remove('open')
   }
});

function toggleModalEditPost() {
   modalEditPost.classList.toggle('open')
};

// Редагування посту
function editPost(postId) {
   modalEditPost.classList.add('open');

   editPostForm.addEventListener('submit', (event) => {
      event.preventDefault();
      const titleInput = document.getElementById('title-edit').value;
      const bodyInput = document.getElementById('body-edit').value;

      cardsInsArr.forEach(cardIns => {
         if (cardIns.postId === postId) {
            cardIns.edit(titleInput, bodyInput);
         }
      });
      modalEditPost.classList.remove('open');
      editPostForm.reset();
   });
}
